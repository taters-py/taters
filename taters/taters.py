import os.path
import StringIO
import sh
import sys
import threading
import subprocess

def null_dest( files ):
    '''Null destination

    Iterates through the given source and does nothing with the resulting files'''

    for f in files:
        pass

def null_filter( files ):
    null_dest( files )

    return []

def debug_dest( files ):
    '''Debug destination

    Outputs the name and data of each file from the given source'''

    for f in files:
        print f.name
        print '-' * len( f.name )
        if not f.delete:
            print f.read()
        else:
            print '* DELETE *'
        print

def debug_filter( files ):
    '''Debug filter

    Outputs the name of each of the files from the given source'''

    for f in files:
        print '+', f.name

        yield f

def read_all_to( f, to, chunk_size = None ):
    '''Reads all the data from the given file and passes each chunk to the given callback function'''

    while True:
        chunk = f.read( *( [ chunk_size ] if chunk_size else [] ) )

        if chunk:
            to( chunk )
        else:
            break

def read_all( f, chunk_size = None ):
    '''Reads and returns all data from the given file'''

    ret = ''
    while True:
        chunk = f.read( *( [ chunk_size ] if chunk_size else [] ) )

        if chunk:
            ret += chunk
        else:
            return ret

def tee( f ):
    '''Takes a file and returns two pipes that both give the files data'''


    p1 = pipe( f.name )
    p2 = pipe( f.name )

    def run():
        def write( chunk ):
            p1.w.write( chunk )
            p2.w.write( chunk )

        read_all_to( f, write )

        p1.w.close()
        p2.w.close()

    threading.Thread( target = run ).start()

    return p1.r, p2.r

class lazy_file( object ):
    '''Lazy file

    A file-like object which only gets opened when it is required (at read or write for instance).'''

    def __init__( self, name, mode = 'rb', *a, **kw ):
        self.name = name
        self._name = name
        self.file = None
        self.size = 0
        self.mode = mode
        self.a = a
        self.kw = kw
        self.delete = False

    def open( self, mode = None ):
        self.file = open( self._name, mode or self.mode, *self.a, **self.kw )
        self.size = os.stat( self._name ).st_size

    def close( self ):
        self.file.close()
        self.file = None
        self.size = 0

    def read( self, *a, **kw ):
        if not self.file:
            self.open( 'rb' )

        return self.file.read( *a, **kw )

    def write( self, data, *a, **kw ):
        if not self.file:
            self.open( 'wb' )

        self.size += len( data )

        return self.file.write( data, *a, **kw )

    def seek( self, *a, **kw ):
        if not self.file:
            self.open()

        return self.file.seek( *a, **kw )

    def tell( self, *a, **kw ):
        if not self.file:
            self.open()

        return self.file.tell( *a, **kw )

    def rename( self, name ):
        self.name = name
        return self

class pipe:
    '''Pipe

    A thread-safe pipe object which allows data to be transferred between threads.
    It provides two file-like endpoints for reading and writing with blocking when
    no data is available or when the pipe is full.'''

    class _reader( object ):
        def __init__( self, pipe ):
            self.pipe = pipe
            self.pos = 0
            self.delete = False

        def read( self, *a, **kw ):
            self.pipe.need_data.set()
            self.pipe.has_data.wait()
            with self.pipe.chunks_lock:
                chunk = self.pipe.chunks.pop( 0 )

                if isinstance( chunk, Exception ):
                    raise chunk
                elif chunk:
                    self.pos += len( chunk )
                    self.pipe.chunk_size -= len( chunk )

                    if self.pipe.chunk_size < self.pipe.chunk_max:
                        self.pipe.not_full.set()

                if not len( self.pipe.chunks ):
                    self.pipe.has_data.clear()

                return chunk

        @property
        def name(self):
            return self.pipe.name

        def rename( self, name ):
            self.pipe.rename( name )
            return self

        def flush( self ):
            pass

    class _writer( object ):
        def __init__( self, pipe ):
            self.pipe = pipe
            self.pos = 0
            self.delete = False

        def write( self, chunk ):
            self.pipe.not_full.wait()
            with self.pipe.chunks_lock:
                self.pipe.chunks.append( chunk )

                if isinstance( chunk, Exception ):
                    pass
                elif chunk:
                    self.pos += len( chunk )
                    self.pipe.chunk_size += len( chunk )

                    if self.pipe.chunk_size >= self.pipe.chunk_max:
                        print 'full!'
                        self.pipe.not_full.clear()

                self.pipe.has_data.set()

        def close( self ):
            self.write( '' )

        @property
        def name(self):
            return self.pipe.name

        def rename( self, name ):
            self.pipe.rename( name )
            return self

        def flush( self ):
            pass

    def __init__( self, name, chunk_max = 8 * 1024 * 1024 ):
        self.chunks = []
        self.chunks_lock = threading.Lock()
        self.chunk_size = 0
        self.chunk_max = chunk_max
        self.need_data = threading.Event()
        self.has_data = threading.Event()
        self.not_full = threading.Event()
        self.not_full.set()
        self.r = self._reader( self )
        self.w = self._writer( self )
        self.name = name
        self.delete = False

    def rename( self, name ):
        self.name = name
        return self

IS_PY3 = sys.version_info[0] == 3
from locale import getpreferredencoding
DEFAULT_ENCODING = getpreferredencoding() or "UTF-8"

def encode_to_py3bytes_or_py2str(s):
    """ takes anything and attempts to return a py2 string or py3 bytes.  this
    is typically used when creating command + arguments to be executed via
    os.exec* """

    fallback_encoding = "utf8"

    if IS_PY3:
        # if we're already bytes, do nothing
        if isinstance(s, bytes):
            pass
        else:
            s = str(s)
            try:
                s = bytes(s, DEFAULT_ENCODING)
            except UnicodeEncodeError:
                s = bytes(s, fallback_encoding)
    else:
        # attempt to convert the thing to unicode from the system's encoding
        try:
            s = unicode(s, DEFAULT_ENCODING)
        # if the thing is already unicode, or it's a number, it can't be
        # coerced to unicode with an encoding argument, but if we leave out
        # the encoding argument, it will convert it to a string, then to unicode
        except TypeError:
            s = unicode(s)

        # now that we have guaranteed unicode, encode to our system encoding,
        # but attempt to fall back to something
        try:
            s = s.encode(DEFAULT_ENCODING)
        except:
            s = s.encode(fallback_encoding, "replace")
    return s

def aggregate_keywords(keywords, sep, prefix, raw=False):
    """ take our keyword arguments, and a separator, and compose the list of
    flat long (and short) arguments.  example

        {'color': 'never', 't': True, 'something': True} with sep '='

    becomes

        ['--color=never', '-t', '--something']

    the `raw` argument indicates whether or not we should leave the argument
    name alone, or whether we should replace "_" with "-".  if we pass in a
    dictionary, like this:

        sh.command({"some_option": 12})

    then `raw` gets set to True, because we want to leave the key as-is, to
    produce:

        ['--some_option=12']

    but if we just use a command's kwargs, `raw` is False, which means this:

        sh.command(some_option=12)

    becomes:

        ['--some-option=12']

    eessentially, using kwargs is a convenience, but it lacks the ability to
    put a '-' in the name, so we do the replacement of '_' to '-' for you.
    but when you really don't want that to happen, you should use a
    dictionary instead with the exact names you want
    """

    processed = []
    encode = encode_to_py3bytes_or_py2str

    for k, v in keywords.items():
        # we're passing a short arg as a kwarg, example:
        # cut(d="\t")
        if len(k) == 1:
            if v is not False:
                processed.append(encode("-" + k))
                if v is not True:
                    processed.append(encode(v))

        # we're doing a long arg
        else:
            if not raw:
                k = k.replace("_", "-")

            if v is True:
                processed.append(encode("--" + k))
            elif v is False:
                pass
            elif sep is None or sep == " ":
                processed.append(encode(prefix + k))
                processed.append(encode(v))
            else:
                arg = encode("%s%s%s%s" % (prefix, k, sep, v))
                processed.append(arg)

    return processed

class process_pipe( pipe ):
    class _reader( pipe._reader ):
        def __init__( self, pipe ):
            self.pipe = pipe
            self.delete = False
            self._name = None

        @property
        def name( self ):
            return self._name or '%s.out' % ( self.pipe.pid or 'not-started-pipe' )

        def rename( self, name ):
            self._name = name
            return self

        def read( self, *a, **kw ):
            if not self.pipe._proc:
                self.pipe.start()

            return self.pipe._proc.stdout.read( *a, **kw )

        def close( self ):
            if not self.pipe._proc:
                self.pipe.start()

            self.pipe._proc.stdout.close()

        def fileno( self ):
            if not self.pipe._proc:
                self.pipe.start()

            return self.pipe.stdout.fileno()

        def flush( self ):
            if not self.pipe._proc:
                self.pipe.start()

            return self.pipe._proc.stdout.flush()

        def connect( self, writer ):
            # setup thread to read from proc to writer
            def run():
                # wait for proc to start
                self.pipe.started.wait()

                read_all_to( self, writer.write )

                writer.close()

            threading.Thread( target = run ).start()

    @property
    def r( self ):
        if not self._r:
            if not self._proc:
                self._r = self._reader( self )
            else:
                raise Exception, 'cannot create reader after proc started'
        return self._r

    class _writer( pipe._writer ):
        def __init__( self, pipe ):
            self.pipe = pipe
            self.delete = False
            self._name = None

        @property
        def name( self ):
            return self._name or '%s.in' % ( self.pipe.pid or 'not-started-pipe' )

        def rename( self, name ):
            self._name = name
            return self

        def write( self, *a, **kw ):
            if not self.pipe._proc:
                self.pipe.start()

            return self.pipe._proc.stdin.write( *a, **kw )

        def close( self ):
            if not self.pipe._proc:
                self.pipe.start()

            self.pipe._proc.stdin.close()

        def fileno( self ):
            if not self.pipe._proc:
                self.pipe.start()

            return self.pipe.stdin.fileno()

        def flush( self ):
            if not self.pipe._proc:
                self.pipe.start()

            return self.pipe._proc.stdin.flush()

        def connect( self, reader ):
            # setup thread to write to proc from reader
            def run():
                # wait for proc to start
                self.pipe.started.wait()

                read_all_to( reader, self.write )

                self.close()

            threading.Thread( target = run ).start()

    @property
    def w( self ):
        if not self._w:
            if not self._proc:
                self._w = self._writer( self )
            else:
                raise Exception, 'cannot create writer after proc started'
        return self._w

    def __init__( self, *a, **kw ):
        self._a = a
        self._kw = kw

        self._r = None
        self._w = None
        self._proc = None

        self.started = threading.Event()

    @property
    def pid( self ):
        if self._proc:
            return self._proc.pid

    def start( self ):
        args = ( list( self._a ) + aggregate_keywords( self._kw, ' ', '--' ) )
        kwargs = {}

        print 'SP: "%s" %s %s' % ( ' '.join( args ), self._r is not None, self._w is not None )

        if self._r:
            kwargs[ 'stdout' ] = subprocess.PIPE
        if self._w:
            kwargs[ 'stdin' ] = subprocess.PIPE

        self._proc = subprocess.Popen( args, **kwargs )

        self.started.set()

    def wait( self ):
        if not self._proc:
            self.start()
        self._proc.wait()

    def rename( self ):
        raise NotImplemented

def uppercase( f ):
    '''Uppercase Builder

    Takes a file and returns its data in uppercase'''

    p = pipe( f.name )

    def run():
        try:
            read_all_to( f, lambda chunk: p.w.write( chunk.upper() ) )
        except Exception as e:
            p.w.write( e )
        p.w.close()

    threading.Thread( target = run ).start()

    return p.r

def sh_builder( cmd, f, *a, **kw ):
    '''general sh builder'''

    print 'B', f.name

    p = process_pipe( cmd, *a, **kw )
    p.w.connect( f )

    return p.r

def lessc( f, *a, **kw ):
    '''less Builder

    Takes a less file and runs it through the lessc compiler and returns the resulting css file.
    Any arguments passed to the function will be passed straight to the sh command'''

    print 'B', f.name

    p = process_pipe( 'lessc', '-', *a, **kw )
    p.w.connect( f )

    return p.r

def sass( f, *a, **kw ):
    '''sass/scss Builder

    Takes a sass/scss file and runs it through the sass compiler and returns the resulting css file.
    Any arguments passed to the function will be passed straight to the sh command.
    Note: to compile scss code you will need to pass the --scss flag'''

    print 'B', f.name

    p = process_pipe( 'sass', '--stdin', *a, **kw )
    p.w.connect( f )

    return p.r

def postcss( f, *a, **kw ):
    '''postcss builder/filter

    Takes a css file and runs it through the postcss css processor and returns the resulting css file.
    Any arguments passed to the function will be passed straight to the sh command'''

    print 'B', f.name

    p = process_pipe( 'postcss', *a, **kw )
    p.w.connect( f )

    return p.r

def uglifyjs( file_paths, *a, **kw ):
    '''JavaScript Builder

    Takes a list of js filenames and passes them to the uglifyjs compiler and returns the resulting css file
    Any arguments passed to the function will be passed straight to the sh command'''

    print 'B', ' + '.join( file_paths )

    p = process_pipe( 'uglifyjs', * file_paths + list( a ), **kw )

    return p.r
